# Covindox Flutter
# TK PBP C03

# PBP-C 2021/2022

## Nama Anggota
> Muhammad Imam Luthfi Balaka 2006524290  
> Elang Permana 2006520405  
> Denny Johannes Hasea 2006531264  
> Fauzan Andri 2006524593  
> Agastya Kenzo Nayandra 2006535905
> Fikri Aufaa Zain 2006484040  
> Michael Daw Balma 2006520746  

## Cerita Daftar Modul  
**1. Modul berita (Elang Permana)**  
**2. Modul User Profile dan Login/Registration systems (Fikri Aufaa Zain)**  
**3. Modul jadwal vaksinasi (Fauzan Andri)**  
**4. Modul artikel (Denny Johannes Hasea)**
Modul ini berisi tentang kumpulan artikel yang dapat dibaca orang ketika senggang yang mungkin dapat menambah wawasan.  
**5. Modul halaman pangaduan (Agastya Kenzo Nayandra)**  
Modul ini akan berisi feedback dari pengguna yang dapat berupa laporan bug di aplikasi atau masukan yang dapat di implementasikan kedepanya
**6. Modul Form Pendaftaran (Muhammad Imam Luthfi Balaka)**  
Modul ini berkaitan dengan modul jadwal vaksinasi, yaitu dalam hal menyediakan form untuk mendaftar pada jadwal yang sudah dipilih pada modul jadwal vaksinasi. Jika sudah mendaftar, maka akan ditampilkan data-data yang sudah diisi Form tersebut akan berkomunikasi secara asinkronus ke halaman web yang sudah dibuat sebelumnya dengan memanfaatkan method yang mengembalikan json dari model terkait.  
**7. Modul text generator penyemangat (Michael Daw Balma)**  



